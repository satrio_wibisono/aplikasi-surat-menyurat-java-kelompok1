/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package kelompok1;

/**
 *
 * @author NandaRusfikri
 */
public class DataRole {
    int id;
    String nama;
 
    public DataRole(int id, String nama)
    {
        this.id = id;
        this.nama = nama;
    }
     
    public int getId()
    {
       return id;
    }
     
    public String getNama()
    {
       return nama;
    }
}
